var app = angular.module("myApp", []);
app.controller("myController", function ($scope) {

    $scope.listStudent = [
        { Name: "Pham Van Anh", Yearofbirth: "1995", Studentcode: "14000123", Major: "Information Technology" },
        { Name: "Nguyen Van Bac", Yearofbirth: "1996", Studentcode: "14000124", Major: "Mathematics" },
        { Name: "Ha Van Anh", Yearofbirth: "1996", Studentcode: "14000125", Major: "Information Technology" },
        { Name: "Mai Tuyet Van", Yearofbirth: "1995", Studentcode: "14000126", Major: "Mathematics-Information" },
        { Name: "Mai Thi Kim Dung", Yearofbirth: "1996", Studentcode: "14000127", Major: "Information " },
        { Name: "Le Van Manh", Yearofbirth: "1996", Studentcode: "14000128", Major: " Technology" },
        { Name: "Cao Van Luong", Yearofbirth: "1996", Studentcode: "14000129", Major: "Physical " },
        { Name: "Do Minh Tuyet", Yearofbirth: "1996", Studentcode: "14000130", Major: "Biological" },

    ];

    $scope.addRow = function () {
        $scope.listStudent.push({ 'Name': $scope.newValue.Name, 'Yearofbirth': $scope.newValue.Yearofbirth, 'Studentcode': $scope.newValue.Studentcode, 'Major': $scope.newValue.Major });
        $scope.newValue.Name = '';
        $scope.newValue.Yearofbirth = '';
        $scope.newValue.Studentcode = '';
        $scope.newValue.Major = '';
    }
    $scope.deleteRow = function (index) {
        $scope.listStudent.splice(index, 1);
    }
    var newValue = {};
    var currentValue = {};
    var currentIndex;
    $scope.submit = true;
    $scope.Edit = function (value, index) {

        $scope.newValue = value;
        old = Object.assign({}, value);
        $scope.currentValue = old;
        currentIndex = index;
        // console.log(old);
        $scope.submit = false;
        $scope.savecancel = true;
    }

    $scope.saveMe = function (newValue) {
        $scope.listStudent[newValue.stt] = newValue;
        $scope.newValue = {};
        $scope.submit = true;
        $scope.savecancel = false;
    }
    $scope.cancelMe = function (currentValue) {
        $scope.listStudent[currentIndex] = currentValue;
        $scope.newValue = {};
        $scope.submit = true;
        $scope.savecancel = false;
    }

});