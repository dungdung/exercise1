var app = angular.module('myApp', []);
app.controller('myController', function ($scope) {

    $scope.showMe = false;
    // $scope.newValue = {};
    $scope.showTimetable = function () {
        $scope.showMe = !$scope.showMe;

    };
    var newValue = {};
    var old = {};
    var currentIndex;
    $scope.list = [
        {id :1, Action: 'Do homework java core', Note: '! important, must do before going to school, do first' },
        {id :2, Action: 'Read book about AngularJS', Note: 'Choose the right book' },
        {id :3, Action: 'Practise to creat a website use AngularJS', Note: 'Make a timetable today' },
        {id :4, Action: 'Learn about the string in java core', Note: 'Prepare the new lesson before class' }

    ];
    $scope.deleteRow = function (i) {
        $scope.list.splice(i, 1);
    };
    $scope.show = false;
    $scope.edit = function (value, index) {
        $scope.newValue = value;
        old  = Object.assign({}, value);
        // console.log(old);
        $scope.old = old;
        currentIndex=index; 

        $scope.show = !$scope.show;
        $scope.hideme = !$scope.hideme;
    };

    $scope.reset = function (old) {
        // for(var i = 0; i < $scope.list.length ; i++) {
        //     if($scope.list[i].id == value.id){
        //         $scope.list[i] = old;
        //     }
        // }
        $scope.list[currentIndex] = old;

        $scope.hideme = !$scope.hideme;
        $scope.show = !$scope.show;

    }
    $scope.addRow = function () {
        $scope.list.push({ 'Action': $scope.action, 'Note': $scope.note });
        $scope.action = '';
        $scope.note = '';
    };
    $scope.saveContact = function (value) {
        $scope.list[value.stt] = value;

        $scope.hideme = !$scope.hideme;
        $scope.show = !$scope.show;
    }
});
